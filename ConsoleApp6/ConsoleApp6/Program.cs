﻿using System;
using ConsoleApp6.Controller;
namespace ConsoleApp6
{
    class Program
    {
        private AbcController a = new AbcController();
        private DefController b = new DefController();
        static void Main(string[] args)
        {
            string str = "Hello World!";

            for ( int i = 0; i < str.Length; i ++)
            {
                ConsoleString();

                Console.WriteLine(str[i]);
            }
        }

        private static void ConsoleString()
        {
            Console.WriteLine("------");
        }
    }
}
